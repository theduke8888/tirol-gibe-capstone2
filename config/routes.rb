Rails.application.routes.draw do
  
  resources :todo_lists
  resources :to_lists
  resources :recipes
  resources :projects




  devise_scope :user do
	  get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
	  get "/sign_up" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration
  end

  devise_for :users, :skip => [:registrations]
  as :user do
	  get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
	  put 'users' => 'devise/registrations#update', :as => 'user_registration'
  end
  root                    to: 'itineraries#index'
  #get '*path',          to: 'home#index', via: :all
  resources :jobs, :products, :categories, :customers
  resources :dashboards
  
  resources :recommendations do
	  member do
		  get 'approve'
		  get 'reject'
	  end
	  collection do
		  get 'for_approval'
		  get 'approved'
		  get 'rejected'
		  
	  end
  end
  
  
  resources :expenses do
	  member do
		  get 'approve'
		  get 'reject'
	  end
	  collection do
		  get 'search_tcn_no'
		  get 'for_approval'
		  get 'approved'
		  get 'rejected'
		 
	  end
  end
  
  resources :activities do
	  member do
		  get 'approve'
		  get 'reject'
	  end
       collection do
            get 'search_control_no'
		  get 'for_approval'
		  get 'approved'
		  get 'rejected'
		
       end
  end
  resources :itineraries do
	  member do
            get 'completed'
		  get 'update_status_visit_list'
		  get 'approve'
		  get 'reject'
	  end
	  collection do
		  get 'current_week'
		  get 'approved'
		  get 'rejected'
		
	  end
	  
  end
  resources :users do
	  collection do
		  get 'show_current_user'
	  end
  end
  
end
