require "application_system_test_case"

class ToListsTest < ApplicationSystemTestCase
  setup do
    @to_list = to_lists(:one)
  end

  test "visiting the index" do
    visit to_lists_url
    assert_selector "h1", text: "To Lists"
  end

  test "creating a To list" do
    visit to_lists_url
    click_on "New To List"

    fill_in "Name", with: @to_list.name
    click_on "Create To list"

    assert_text "To list was successfully created"
    click_on "Back"
  end

  test "updating a To list" do
    visit to_lists_url
    click_on "Edit", match: :first

    fill_in "Name", with: @to_list.name
    click_on "Update To list"

    assert_text "To list was successfully updated"
    click_on "Back"
  end

  test "destroying a To list" do
    visit to_lists_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "To list was successfully destroyed"
  end
end
