jobs = Job.create([
     {
          title: 'professional sales rep'
     },
     {
          title: 'marketing specialist'
     },
     {
          title: 'collection staff'
     },
     {
          title: 'technical specialist'
     },
     {
          title: 'product specialist'
     },
     {
          title: 'chief executive officer'
     },
     {
          title: 'vice president'
     }
])

User.create!(
	  lastname:		'Tirol',
	  firstname: 		'Gibe',
	  mint: 			'S',
	  email:			'gibe@instatech.net.ph',
	  contactnumber:	'09323298787',
	  job_id:			4,
	  address:		"Davao City",
	  admin2:			true

)

User.create!(
	  lastname:		'Villacencio',
	  firstname: 		'Walter',
	  mint: 			'S',
	  email:			'walter@instatech.net.ph',
	  contactnumber:	'082-296-1191',
	  job_id:			6,
	  address:		"Davao City",
	  admin:			true

)
User.create!(
	  lastname:		'Villacencio',
	  firstname: 		'Lanie',
	  mint: 			'',
	  email:			'lanie@instatech.net.ph',
	  contactnumber:	'082-296-1191',
	  job_id:			7,
	  address:		"Davao City",
	  admin:			true

)


15.times do |n|
     User.create!(
          lastname:                 Faker::Name.last_name,
          firstname:                Faker::Name.first_name,
          mint:                     Faker::Name.middle_name,
          email:                    "email#{n+1}@instatech.net.ph",
          contactnumber:            '082-296-1191',
          job_id:                   rand(1..5),
          address:                  'Davao City'

     )
end

50.times do |n|
	Customer.create!(
		company:                 Faker::Company.name,
		year_establised:         rand(1960..2020),
		no_employees:            rand(5..100),
		business_address:        Faker::Address.full_address,
		tel_no:                  Faker::PhoneNumber.phone_number,
		fax_no:                  Faker::PhoneNumber.phone_number,
		business_location:       %w[Leased Owned].sample,
		business_year_stay:      rand(1..20),
		residential_address:     Faker::Address.full_address,
		residential_location:    %w[Leased Owned].sample,
		residential_tel_no:      Faker::PhoneNumber.phone_number,
		residential_year_stay:   rand(1950..2020),
		type_business:           Faker::Company.industry,
		product_lines:           Faker::Commerce.material,
		industry_affiliation:    Faker::Company.type,
		position:                Faker::Job.title,
		form_owner_name:         nil,
		form_partner:            nil,
		form_corp_pres:          nil,
		form_corp_vp:            nil,
		form_corp_sec:           nil,
		form_corp_tres:          nil,
		form_corp_dir:           nil
	)
end

=begin
Category.create!([
				    { name: 'anesthesia' },
				    { name: 'critical care' },
				    { name: 'gastroenterology' },
				    { name: 'infenction control' },
				    { name: 'infusion theraphy' },
				    { name: 'medical sundries' },
				    { name: 'patient identification' },
				    { name: 'personal protective equipment' },
				    { name: 'respiratory' },
				    { name: 'surgical' },
				    { name: 'urology' },
				    { name: 'patient monitoring' }
			  ])

Product.create!([
				   {name: 'CVP Manometer', category_id: 1},
				   {name: 'Essenzial Endotracheal Tube Holder',category_id: 1},
				   {name: 'ESSENZIAL Endotracheal Tube Series', category_id: 1},
				   {name: 'Medispine Spinal Needle', category_id: 1},
				   {name: 'Video Laryngoscope Series', category_id: 1},
				   {name: 'Essenzial SPO2 Sensor', category_id: 2},
				   {name: 'Medico ECG Electrodes (MSGLT-01)', category_id: 2},
				   {name: 'Medico ECG Electrodes (MSGLT-03GRT)', category_id: 2},
				   {name: 'Medico ECG electrodes (MSGST-07)', category_id: 2},
				   {name: 'Medico ECG Electrodes MSGST-64', category_id: 2},
				   {name: 'Trace ECG Paper', category_id: 2},
				   {name: 'ESSENZIAL INFANT FEEDING TUBE', category_id: 3},
				   {name: 'ESSENZIAL STOMACH TUBE', category_id: 3},
				   {name: 'ESSENZIAL STOMACH TUBE (SILICON)', category_id: 3},
				   {name: 'Essenzial Safety Box & Sharps Container', category_id: 4},
				   {name: 'Povidone Iodine', category_id: 4},
				   {name: 'Sanimat Self Adhesive Decontaminating Mat', category_id: 4},
				   {name: 'Sterilization Pouch', category_id: 4},
				   {name: 'Essenzial Needle', category_id: 5},
				   {name: 'Essenzial Syringe', category_id: 5},
				   {name: 'Gloreg IV Flow Regulator', category_id: 5},
				   {name: 'Insta Inlet Fitter', category_id: 5},
				   {name: 'Insta Inlet Fitter Extension Tubings', category_id:5 },
				   {name: 'Instaflon IV Cannula', category_id: 5},
				   {name: 'Instaflow Infusion Sets', category_id: 5},
				   {name: 'Instafuse Blood Transfusion Set', category_id: 5},
				   {name: 'Instafuse Infusion Pump', category_id: 5},
				   {name: 'Instafuse Syringe Pump', category_id: 5},
				   {name: 'Instaneo IV Cannula', category_id: 5},
				   {name: 'Instavols Volumetric Set series', category_id: 5},
				   {name: 'Kethin IV Cannula', category_id: 5},
				   {name: 'Mediflex 3-way stop cock', category_id: 5},
				   {name: 'Mediflexo Extension Tubeing with 3-way stopcock', category_id: 5},
				   {name: 'Mediflon IV Cannula', category_id: 4},
				   {name: 'Mediviens Extension Tubing', category_id: 5},
				   {name: 'Newfix IV Dressing', category_id: 5},
				   {name: 'Newfix IV splint series', category_id: 5},
				   {name: 'DISPOSABLE PILLOW', category_id: 6},
				   {name: 'Elastic Bandage', category_id: 6},
				   {name: 'Essenzial Specimen Container', category_id: 6},
				   {name: 'Essenzial Umbilical Cord Clamp', category_id: 6},
				   {name: 'essenzial comfort ID bracelet', category_id: 7},
				   {name: 'essenzial ID bracelet', category_id: 7},
				   {name: 'essenzial safeband', category_id: 7},
				   {name: 'essenzial safeband printer', category_id: 7},
				   {name: 'Essenzial Bouffant Cap', category_id: 8},
				   {name: 'Essenzial Facemask 3-ply tie-on with shield', category_id: 8},
				   {name: 'Facemask with filter and shield', category_id: 8},
				   {name: 'Isolation Gown', category_id: 8},
				   {name: 'Isolation Hood', category_id: 8},
				   {name: 'Shoe Cover', category_id: 8},
				   {name: 'Essenzial Guedel Airway', category_id: 9},
				   {name: 'ESSENZIAL NASAL OXYGEN CANNULA', category_id: 9},
				   {name: 'ESSENZIAL NEBULIZING KIT', category_id: 9},
				   {name: 'ESSENZIAL PREFILLED BUBBLE HUMIDIFIER', category_id: 9},
				   {name: 'Essenzial Suction Catheter', category_id: 9},
				   {name: 'MEDICISER respitatory exeercisor', category_id: 9},
				   {name: 'Essenzial Connecting Tube', category_id: 10},
				   {name: 'laparotomy Pack', category_id: 10},
				   {name: 'Newmed staple remover', category_id: 10},
				   {name: 'Newmed Skin Stapler', category_id: 10},
				   {name: 'Newmed Surgical Plaster', category_id: 10},
				   {name: 'Newpore Surgical Tape (Hypoallergenic)', category_id: 10},
				   {name: 'NON WOVEN PADS', category_id: 10},
				   {name: 'Relyon Sutures', category_id: 10},
				   {name: 'SURGICAL', category_id: 10},
				   {name: 'SURGICAL BLADE', category_id: 10},
				   {name: 'Surgicare  PREMIER  Surgical Gloves', category_id: 10},
				   {name: 'ESSENZIAL FOLEY BALLOON', category_id: 11},
				   {name: 'Essenzial Foley Balloon Catheter Silicone, Needleless', category_id: 11},
				   {name: 'Essenzial Nelaton Catheter', category_id: 11},
				   {name: 'Essenzial Pediatric  Urine Collector', category_id: 11},
				   {name: 'Essenzial Urine Drainage Bag', category_id: 11}
			 ])



=end

Category.create!([
	                   { name: 'anesthesia', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620322336/CVP_Manometer_szd6qt.jpg") },
	                   { name: 'critical care', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620325883/Medico_ECG_Electrodes_MSGLT-01_outgvj.jpg") },
	                   { name: 'gastroenterology', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326174/Essenzial_Infant_Feeding_Tube_tiba3e.jpg") },
	                   { name: 'infenction control',  image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326274/Essenzial_Safety_Box_Sharps_Container_xvvzyr.jpg") },
	                   { name: 'infusion theraphy', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326456/Essenzial_Disposable_Needle_ijexwe.jpg") },
	                   { name: 'medical sundries', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327129/Urine_Specimen_Container_60ml_Php_700.00_per_pack_100_s_wsrwp4.jpg") },
	                   { name: 'patient identification', image: open('https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327301/Essenzial_ID_Bracelet_bwhrhu.jpg') },
	                   { name: 'personal protective equipment', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327333/Essenzial_Bouffant_Cap_jdjrup.jpg") },
	                   { name: 'respiratory', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327569/Essenzial_Nebulizing_Kit_with_Mask_o0sisk.jpg") },
	                   { name: 'surgical', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327711/Newmed_Skin_Staple_Remover_oktawn.jpg") },
	                   { name: 'urology', image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327990/Essenzial_Foley_Balloon_Catheter_Latex-free_Silicone_Needle-Free_s8gxbi.jpg") },
				    
                 ])

Product.create!([
	                  {name: 'CVP Manometer', category_id: 1, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620322336/CVP_Manometer_szd6qt.jpg")},
	                  {name: 'Essenzial Endotracheal Tube Holder',category_id: 1, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620322445/Essenzial_Endotrachael_Tube_Holder_gyjjpn.jpg")},
	                  {name: 'ESSENZIAL Endotracheal Tube Series', category_id: 1, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620322519/Essenzial_Endotreal_Tube_with_stylet_ujbexq.jpg")},
	                  {name: 'Medispine Spinal Needle', category_id: 1, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620322636/Medispine_Spinal_Needle_mtatal.jpg")},
	                  {name: 'Essenzial SPO2 Sensor', category_id: 2, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620325856/Essenzial_SPO2_Sensor_Nellcor_nolfqd.jpg")},
	                  {name: 'Medico ECG Electrodes (MSGLT-01)', category_id: 2, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620325883/Medico_ECG_Electrodes_MSGLT-01_outgvj.jpg")},
	                  {name: 'Medico ECG Electrodes (MSGLT-03GRT)', category_id: 2, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620325894/Medico_ECG_Electrodes_MSGLT-03GRT_a5wkf2.jpg")},
	                  {name: 'Medico ECG electrodes (MSGST-07)', category_id: 2, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620325902/Medico_ECG_Electrodes_MSGST-07_xkkaq4.jpg")},
	                  {name: 'Medico ECG Electrodes MSGST-64', category_id: 2, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620325918/Medico_ECG_Electrodes_MSGST-64_lovp7i.jpg")},
	                  {name: 'Trace ECG Paper', category_id: 2, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620325939/Trace_ECG_paper_pylwnt.jpg")},
	                  {name: 'ESSENZIAL INFANT FEEDING TUBE', category_id: 3, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326174/Essenzial_Infant_Feeding_Tube_tiba3e.jpg")},
	                  {name: 'ESSENZIAL STOMACH TUBE', category_id: 3, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326200/Essenzial_Stomach_Tube_Silicone_irptra.jpg")},
	                  {name: 'ESSENZIAL STOMACH TUBE (SILICON)', category_id: 3, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326201/Essenzial_Stomach_Tube_xuumfp.jpg")},
	                  {name: 'Essenzial Safety Box & Sharps Container', category_id: 4, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326274/Essenzial_Safety_Box_Sharps_Container_xvvzyr.jpg")},
	                  {name: 'Povidone Iodine', category_id: 4, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326286/Povidone_Iodine_xdkgdb.jpg")},
	                  {name: 'Sanimat Self Adhesive Decontaminating Mat', category_id: 4},
	                  {name: 'Sterilization Pouch', category_id: 4, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326332/Sterilization_Pouch_ogpita.jpg")},
	                  {name: 'Essenzial Needle', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326456/Essenzial_Disposable_Needle_ijexwe.jpg")},
	                  {name: 'Essenzial Syringe', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326475/Essenzial_Syringe_jegcl8.jpg")},
	                  {name: 'Gloreg IV Flow Regulator', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326475/Essenzial_Syringe_jegcl8.jpg")},
	                  {name: 'Insta Inlet Fitter', category_id: 5,image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326512/Insta_Inlet_Fitter_Injection_Stopper_ifxqsh.jpg")},
	                  {name: 'Insta Inlet Fitter Extension Tubings', category_id:5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326521/Insta_Inlet_Fitter_Extension_Tubing_2-ports_Needlefree_2_vnxioh.jpg") },
	                  {name: 'Instaflon IV Cannula', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326548/INSTAFLON_IV_CANNULA_G-18_G-20_G-22_G-24_._2_yey4af.jpg")},
	                  {name: 'Instaflow Infusion Sets', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326572/Instaflow_Infusion_Set_series_odvlcf.jpg")},
	                  {name: 'Instafuse Blood Transfusion Set', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326598/Instafuse_Blood_Transfusion_Set_2_sbyv5x.jpg")},
	                  {name: 'Instafuse Infusion Pump', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326598/Instafuse_Blood_Transfusion_Set_2_sbyv5x.jpg")},
	                  {name: 'Instafuse Syringe Pump', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326724/Instafuse_Infusion_Pump_bs2flz.jpg")},
	                  {name: 'Instaneo IV Cannula', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326744/Instafuse_Syringe_Pump_ipcagx.jpg")},
	                  {name: 'Instavols Volumetric Set series', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326763/Instaneo_IV_Cannula_G-24_G-26_nu2syv.jpg")},
	                  {name: 'Kethin IV Cannula', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326784/KETHIN_IV_CANNULA_G-16_G-18_G-20_G-22_G-24_obvzib.jpg")},
	                  {name: 'Mediflex 3-way stop cock', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326923/3-_way_stopcock_Php_400.00_per_pack_10_s_m6lpsf.jpg")},
	                  {name: 'Mediflexo Extension Tubeing with 3-way stopcock', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326926/3-_way_stopcock_with_10cm_extension_tubing_Php550.00_per_10_s_v9bp1t.jpg")},
	                  {name: 'Mediflon IV Cannula', category_id: 4, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326951/MEDIFLON_IV_CANNULA_G-18_G-20_G-22_G-24_2_mxsx51.jpg")},
	                  {name: 'Mediviens Extension Tubing', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620326999/Insta_Inlet_Fitter_Extension_Tubing_2-ports_Needlefree_2_titdwp.jpg")},
	                  {name: 'Newfix IV Dressing', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327010/NEWFIX_IV_DRESSING_2_pcpdo8.jpg")},
	                  {name: 'Newfix IV splint series', category_id: 5, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327097/NEWFIX_IV_ARMBOARD_2_cj5vvv.jpg")},
	                  {name: 'DISPOSABLE PILLOW', category_id: 6, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327103/Disposable_Pillow_mjyyb9.jpg")},
	                  {name: 'Elastic Bandage', category_id: 6, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327213/Elastic_Bandage_dphmc1.jpg")},
	                  {name: 'Urine Specimen Container', category_id: 6, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327129/Urine_Specimen_Container_60ml_Php_700.00_per_pack_100_s_wsrwp4.jpg")},
	                  {name: 'Essenzial Umbilical Cord Clamp', category_id: 6, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327167/Umbilical_Cord_Clamp_Php_700.00_per_pack_100_s_iwo7pe.jpg")},
	                  {name: 'essenzial comfort ID bracelet', category_id: 7, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327273/Essenzial_Comfort_ID_Bracelet_ee84tp.jpg")},
	                  {name: 'essenzial ID bracelet', category_id: 7, image: open('https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327301/Essenzial_ID_Bracelet_bwhrhu.jpg')},
	                  {name: 'essenzial safeband', category_id: 7, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327315/Essenzial_Safeband_printer_u9nzof.jpg")},
	                  {name: 'essenzial safeband printer', category_id: 7, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327322/Essenzial_Safeband_ID_Bracelet_xijtr8.jpg")},
	                  {name: 'Essenzial Bouffant Cap', category_id: 8, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327333/Essenzial_Bouffant_Cap_jdjrup.jpg")},
	                  {name: 'Essenzial Facemask 3-ply tie-on with shield', category_id: 8, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327423/Essenzial_facemask_3ply_tie-on_with_shield_sgkbtv.jpg")},
	                  {name: 'Facemask with filter and shield', category_id: 8, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327414/Essenzial_facemask_3ply_tie-on_with_shield_hmffv0.jpg")},
	                  {name: 'Isolation Gown', category_id: 8, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327431/Isolation_Gown_prnygg.jpg")},
	                  {name: 'Isolation Hood', category_id: 8, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327439/Isolation_Hood_p1nnzw.jpg")},
	                  {name: 'Shoe Cover', category_id: 8, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327447/shoe_cover_aajdct.jpg")},
	                  {name: 'Essenzial Guedel Airway', category_id: 9, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327543/Essenzial_Gudel_Airway_nfe1jz.jpg")},
	                  {name: 'ESSENZIAL NASAL OXYGEN CANNULA', category_id: 9, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327554/Essenzial_Nasal_Oxygen_Cannula_x6tpvz.jpg")},
	                  {name: 'ESSENZIAL NEBULIZING KIT', category_id: 9, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327569/Essenzial_Nebulizing_Kit_with_Mask_o0sisk.jpg")},
	                  {name: 'ESSENZIAL PREFILLED BUBBLE HUMIDIFIER', category_id: 9, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327578/Essenzial_Prefilled_Bubble_Humidifier_igb7pu.jpg") },
	                  {name: 'Essenzial Suction Catheter', category_id: 9, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327593/Essenzial_Suction_Catheter_voxuon.jpg")},
	                  {name: 'MEDICISER respitatory exeercisor', category_id: 9, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327651/Mediciser_Respiratory_Exerciser_tyogf5.jpg")},
	                  {name: 'Essenzial Connecting Tube', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327669/Essenzial_Suction_Connecting_Tube_with_handle_Pooledrain_rnp4vs.jpg")},
	                  {name: 'laparotomy Pack', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327705/Laparostomy_Pack_lm1wqe.jpg")},
	                  {name: 'Newmed staple remover', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327711/Newmed_Skin_Staple_Remover_oktawn.jpg")},
	                  {name: 'Newmed Skin Stapler', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327722/Newmed_Skin_Stapler_qsa4wq.jpg")},
	                  {name: 'Newmed Surgical Plaster', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327801/Surgical_Plaster_Hospital_Size_Php_699.00_per_tube_l0qx69.jpg")},
	                  {name: 'Newpore Surgical Tape (Hypoallergenic)', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327791/Surgical_Plaster_Hypoallergenic_1inch_Php_405.00_per_box_12_s_frg1dm.jpg")},
	                  {name: 'NON WOVEN PADS', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327886/Essenzial_NonWoven_Pads_n0l6o6.jpg")},
	                  {name: 'Relyon Sutures', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327919/Relyon_Sutures_arxvdx.jpg")},
	                  {name: 'SURGICAL BLADE', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327935/Surgical_Blade_kjfdzy.jpg")},
	                  {name: 'Surgicare  PREMIER  Surgical Gloves', category_id: 10, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327974/Surgicare_Premier_Surgical_Gloves_behxdg.jpg")},
	                  {name: 'ESSENZIAL FOLEY BALLOON', category_id: 11, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327990/Essenzial_Foley_Balloon_Catheter_Latex-free_Silicone_Needle-Free_s8gxbi.jpg")},
	                  {name: 'Essenzial Foley Balloon Catheter Silicone, Needleless', category_id: 11, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620327995/Essenzial_Foley_Balloon_Catheter_Latex_Silicone_Coated_Needle-Free_o0kmrl.jpg")},
	                  {name: 'Essenzial Nelaton Catheter', category_id: 11, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620328003/Essenzial_Nelaton_Straight_Catheter_jyfsib.jpg")},
	                  {name: 'Essenzial Pediatric  Urine Collector', category_id: 11, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620328013/Essenzial_Pediatric_Urine_Collector_rqpxdt.jpg")},
	                  {name: 'Essenzial Urine Drainage Bag', category_id: 11, image: open("https://res.cloudinary.com/dzhwgvtsm/image/upload/v1620328060/Essenzial_Urine_Drainage_Bag_2000ml_vegn56.jpg")}

                ])




10.times do |n|
	Itinerary.create!(
		  iti_period_from: 		Time.zone.now.beginning_of_week,
		  iti_period_to:		Time.zone.now.end_of_week,
		  iti_created: 		true,
		  user_id: 			rand(4..18)
=begin
		 
			  visit_lists_attributes: [
				    {
					
							 
								   visit_date:		rand(Time.zone.now.beginning_of_week..Time.zone.now.end_of_week),
								   visit_account: 	Customer.order(Arel.sql('RANDOM()')).first.company,
								   visit_person: 	Faker::Name.name,
								   visit_place: 	"Davao City"
							 
				    }
				   
			  ]
=end
 
	
	)
end

12.times do |x|
	rand(10..30).times do |y|
		VisitList.create!(
			  visit_date:		rand(Time.zone.now.beginning_of_week..Time.zone.now.end_of_week),
			  visit_account: 	Customer.order(Arel.sql('RANDOM()')).first.company,
			  visit_person: 	Faker::Name.name,
			  visit_place: 	"Davao City",
			  itinerary_id:	rand(1..10)
		
		)
	end
end

12.times do |n|
	d = rand(10..50)
	d2 = rand(20..50)
	Activity.create!(
		  act_rep_name: 				Faker::Name.name,
		  act_rep_title: 				['professional sales representative', 'marketing specialist', 'collection staff', 'technical specialist', 'product specialist'].sample,
		  act_rep_department: 			['Marketing', 'Technical', 'Sales'].sample,
		  act_rep_head: 				Faker::Name.name,
		  act_event_activity: 			['Product Demo', 'Trade Fair'].sample,
		  act_event_objective: 			'Introduce new products',
		  act_event_product_line_up: 		Product.order(Arel.sql('RANDOM()')).first.name,
		  act_event_target: 			'New products will be used in the hospital',
		  act_event_time_frame: 			['1 week', '2 weeks', '3 weeks', '1 month', '1 - 5 days'].sample,
		  act_venue_place: 				['Robinso Maill', 'SM City Davao', 'Abreeza Mall', 'San Pedro Hospital', 'Souther Medical Center', 'Davao Doctors Hosptial' ].sample,
		  act_venue_department: 			['Trade Fair', 'Ground'].sample,
		  act_venue_participants: 		rand(10..20),
		  user_id: 					rand(4..18),
		  act_control_no: 				"ACN#{Date.today.year}-#{n}",
		  act_venue_date:				d.days.from_now,
		  act_date_needed:				d2.days.from_now
	
	
	)
end

15.times do
	rand(10..30).times do
		Interest.create!(
			  int_cust_name: 			Faker::Name.name,
			  int_position: 			Faker::Job.position,
			  int_function: 			Faker::Job.title,
			  int_contribution: 		['Bought products', 'Sign contract', 'Ask for quoation'].sample,
			  activity_id: 			rand(1..12)
		)
	end
end

10.times do
	rand(10..30).times do
		Support.create!(
			  sup_materials: 		Faker::Construction.material,
			  sup_quantity: 		rand(1..20),
			  sup_budget: 			rand(1000..5000),
			  sup_specification: 	"Contruction",
			  activity_id: 		rand(1..12)
		 )
	end
end

20.times do |n|
	d = rand(1..4)
	Expense.create!(
		  exp_period_from:			d.weeks.ago.beginning_of_week,
		  exp_period_to: 			d.weeks.ago.end_of_week,
		  user_id: 				rand(1..7),
		  exp_tcn: 				"TCN#{Date.today.year}-#{n}"
	)
end

10.times do
	rand(5..10).times do
		expense_id = rand(1..20)
		Report.create!(
			  
			  rep_particular: 		["Introduce new products", "Visit new customers", "Visit client"].sample,
			  rep_transpo: 		rand(500.1000),
			  rep_promo: 			rand(500.1000),
			  rep_repre: 			rand(100..500),
			  rep_mis: 			rand(100.700),
			  expense_id:			expense_id,
			  rep_date:			rand(Expense.find(expense_id).exp_period_from..Expense.find(expense_id).exp_period_to)
		
		)
	end
end


