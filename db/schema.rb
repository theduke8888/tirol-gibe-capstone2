# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_07_191407) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "act_rep_name"
    t.string "act_rep_title"
    t.string "act_rep_department"
    t.string "act_rep_head"
    t.string "act_event_activity"
    t.string "act_event_objective"
    t.string "act_event_product_line_up"
    t.string "act_event_target"
    t.string "act_event_time_frame"
    t.string "act_venue_place"
    t.string "act_venue_department"
    t.integer "act_venue_participants", default: 0
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "act_control_no"
    t.date "act_date_needed"
    t.datetime "act_venue_date"
    t.boolean "act_status", default: false
    t.boolean "act_rejected", default: false
    t.index ["act_control_no"], name: "index_activities_on_act_control_no", unique: true
    t.index ["user_id"], name: "index_activities_on_user_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "image"
  end

  create_table "customers", force: :cascade do |t|
    t.string "company"
    t.integer "year_establised"
    t.integer "no_employees"
    t.string "business_address"
    t.string "tel_no"
    t.string "fax_no"
    t.string "business_location"
    t.string "business_year_stay"
    t.string "residential_address"
    t.string "residential_location"
    t.integer "residential_year_stay"
    t.string "type_business"
    t.string "product_lines"
    t.string "industry_affiliation"
    t.string "position"
    t.string "form_owner_name"
    t.string "form_partner"
    t.string "form_corp_pres"
    t.string "form_corp_vp"
    t.string "form_corp_sec"
    t.string "form_corp_tres"
    t.string "form_corp_dir"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "residential_tel_no"
  end

  create_table "expenses", force: :cascade do |t|
    t.datetime "exp_period_from"
    t.datetime "exp_period_to"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "exp_tcn"
    t.boolean "exp_rejected", default: false
    t.boolean "exp_status", default: false
    t.index ["exp_tcn"], name: "index_expenses_on_exp_tcn", unique: true
    t.index ["user_id"], name: "index_expenses_on_user_id"
  end

  create_table "interests", force: :cascade do |t|
    t.string "int_cust_name"
    t.string "int_position"
    t.string "int_function"
    t.string "int_contribution"
    t.bigint "activity_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["activity_id"], name: "index_interests_on_activity_id"
  end

  create_table "items", force: :cascade do |t|
    t.string "item_name"
    t.decimal "item_stand_price", precision: 6, scale: 2
    t.decimal "item_compe_price", precision: 6, scale: 2
    t.string "item_compe_brand"
    t.decimal "item_recom_price", precision: 6, scale: 2
    t.integer "item_quan_order"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "recommendation_id"
    t.index ["recommendation_id"], name: "index_items_on_recommendation_id"
  end

  create_table "itineraries", force: :cascade do |t|
    t.date "iti_period_from"
    t.date "iti_period_to"
    t.boolean "iti_status", default: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "iti_rejected", default: false
    t.boolean "iti_created", default: false
    t.index ["user_id"], name: "index_itineraries_on_user_id"
  end

  create_table "jobs", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.string "sku"
    t.string "image"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "category_id", null: false
    t.index ["category_id"], name: "index_products_on_category_id"
    t.index ["sku"], name: "index_products_on_sku", unique: true
  end

  create_table "recommendations", force: :cascade do |t|
    t.string "rec_account_name"
    t.string "rec_contact_person"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "rec_terms"
    t.boolean "rec_rejected", default: false
    t.boolean "rec_status", default: false
    t.index ["user_id"], name: "index_recommendations_on_user_id"
  end

  create_table "reports", force: :cascade do |t|
    t.date "rep_date"
    t.string "rep_particular"
    t.decimal "rep_transpo", precision: 8, scale: 2
    t.decimal "rep_promo", precision: 8, scale: 2
    t.decimal "rep_repre", precision: 8, scale: 2
    t.decimal "rep_mis", precision: 8, scale: 2
    t.bigint "expense_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["expense_id"], name: "index_reports_on_expense_id"
  end

  create_table "supports", force: :cascade do |t|
    t.string "sup_materials"
    t.integer "sup_quantity"
    t.decimal "sup_budget", precision: 8, scale: 2
    t.string "sup_specification"
    t.bigint "activity_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["activity_id"], name: "index_supports_on_activity_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.bigint "todo_list_id", null: false
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["todo_list_id"], name: "index_tasks_on_todo_list_id"
  end

  create_table "todo_lists", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "lastname"
    t.string "firstname"
    t.string "mint"
    t.string "contactnumber"
    t.boolean "admin", default: false
    t.bigint "job_id"
    t.string "address"
    t.string "image"
    t.boolean "admin2", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "visit_lists", force: :cascade do |t|
    t.date "visit_date"
    t.string "visit_place"
    t.string "visit_account"
    t.string "visit_person"
    t.boolean "visit_completed", default: false
    t.bigint "itinerary_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["itinerary_id"], name: "index_visit_lists_on_itinerary_id"
  end

  add_foreign_key "activities", "users"
  add_foreign_key "expenses", "users"
  add_foreign_key "interests", "activities"
  add_foreign_key "items", "recommendations"
  add_foreign_key "itineraries", "users"
  add_foreign_key "products", "categories"
  add_foreign_key "recommendations", "users"
  add_foreign_key "reports", "expenses"
  add_foreign_key "supports", "activities"
  add_foreign_key "tasks", "todo_lists"
  add_foreign_key "users", "jobs"
  add_foreign_key "visit_lists", "itineraries"
end
