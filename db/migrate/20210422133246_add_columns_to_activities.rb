class AddColumnsToActivities < ActiveRecord::Migration[6.0]
  def change
    add_column :activities, :act_status,:boolean, default: false
    add_column :activities, :act_rejected, :boolean , default: false
  end
end
