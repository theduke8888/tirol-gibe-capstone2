class RenameColumnCustomers < ActiveRecord::Migration[6.0]
  def change
    rename_column :customers, :residentail_tel_no, :residential_tel_no
  end
end
