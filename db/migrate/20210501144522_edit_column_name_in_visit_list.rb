class EditColumnNameInVisitList < ActiveRecord::Migration[6.0]
	def change
		rename_column :visit_lists, :visit, :visit_place
	end
end
