class AddRejectedToItineraries < ActiveRecord::Migration[6.0]
  def change
    add_column :itineraries,:rejected, :boolean, default: false
  end
end
