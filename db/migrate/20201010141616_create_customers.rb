class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :company
      t.integer :year_establised
      t.integer :no_employees
      t.string :business_address
      t.string :tel_no
      t.string :fax_no
      t.string :business_location
      t.string :business_year_stay
      t.string :residential_address
      t.string :residential_location
      t.integer :residential_year_stay
      t.string :type_business
      t.string :product_lines
      t.string :industry_affiliation
      t.string :position
      t.string :form_owner_name
      t.string :form_partner
      t.string :form_corp_pres
      t.string :form_corp_vp
      t.string :form_corp_sec
      t.string :form_corp_tres
      t.string :form_cor_dir

      t.timestamps
    end
  end
end
