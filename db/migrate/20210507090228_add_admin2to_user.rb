class AddAdmin2toUser < ActiveRecord::Migration[6.0]
  def change
	  add_column :users, :admin2, :boolean, default: false
  end
end
