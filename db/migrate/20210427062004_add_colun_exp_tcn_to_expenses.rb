class AddColunExpTcnToExpenses < ActiveRecord::Migration[6.0]
  def change
    add_column :expenses, :exp_tcn, :string
    add_index :expenses, :exp_tcn
  end
end
