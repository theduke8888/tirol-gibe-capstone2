class EditColumnsInExpenses < ActiveRecord::Migration[6.0]
	def change
		change_column :expenses, :exp_period_to, :timestamp, using: 'exp_period_to::timestamp without time zone'
		change_column :expenses, :exp_period_from, :timestamp, using: 'exp_period_from::timestamp without time zone'
	end
end
