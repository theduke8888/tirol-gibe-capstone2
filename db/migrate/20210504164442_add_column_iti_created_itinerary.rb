class AddColumnItiCreatedItinerary < ActiveRecord::Migration[6.0]
  def change
	  add_column :itineraries, :iti_created, :boolean, default: false
  end
end
