class AddForeignKeyToUsers < ActiveRecord::Migration[6.0]
  def change
      add_column :users, :job_id, :bigint
      add_foreign_key :users, :jobs
  end
end
