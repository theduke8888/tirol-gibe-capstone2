class EditColumnInSupports2 < ActiveRecord::Migration[6.0]
  def change
    change_column :supports, :sup_budget, :float, :precision => 8, :scale => 2
  end
end
