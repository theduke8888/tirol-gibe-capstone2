class CreateExpenses < ActiveRecord::Migration[6.0]
  def change
    create_table :expenses do |t|
      t.string :exp_period_from
      t.string :exp_period_to
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
