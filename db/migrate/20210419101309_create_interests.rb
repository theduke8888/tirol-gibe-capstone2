class CreateInterests < ActiveRecord::Migration[6.0]
  def change
    create_table :interests do |t|
      t.string :int_cust_name
      t.string :int_position
      t.string :int_function
      t.string :int_contribution
      t.references :activity, null: false, foreign_key: true

      t.timestamps
    end
  end
end
