class AddIndexToActivity < ActiveRecord::Migration[6.0]
  def change
	  add_index :activities, :act_control_no, unique: true
  end
end
