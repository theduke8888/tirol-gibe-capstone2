class AddColumnsToUsers < ActiveRecord::Migration[6.0]
	def change
		add_column :users, :lastname, :string
		add_column :users, :firstname, :string
		add_column :users, :mint, :string
		add_column :users, :contactnumber, :string
		add_column :users, :admin, :boolean, default: false	
	end
end