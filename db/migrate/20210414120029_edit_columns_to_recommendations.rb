class EditColumnsToRecommendations < ActiveRecord::Migration[6.0]
  def change
    change_column :recommendations, :rec_stand_price, :float, precision: 8, scale: 2
    change_column :recommendations, :rec_compe_price, :float, precision: 8, scale: 2
    change_column :recommendations, :rec_recom_price, :float, precision: 8, scale: 2

  end
end
