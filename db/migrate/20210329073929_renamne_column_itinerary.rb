class RenamneColumnItinerary < ActiveRecord::Migration[6.0]
  def change
    rename_column :itineraries, :rejected, :iti_rejected
  end
end
