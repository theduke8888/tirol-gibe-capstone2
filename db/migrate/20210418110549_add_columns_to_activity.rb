class AddColumnsToActivity < ActiveRecord::Migration[6.0]
  def change
    add_column :activities, :act_control_no, :string
    add_column :activities, :act_date_request, :date
    add_column :activities, :act_date_needed, :date
  end
end
