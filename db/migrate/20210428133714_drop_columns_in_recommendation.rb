class DropColumnsInRecommendation < ActiveRecord::Migration[6.0]
  def change
	  remove_column :recommendations, :rec_item
	  remove_column :recommendations, :rec_stand_price
	  remove_column :recommendations, :rec_compe_price
	  remove_column :recommendations, :rec_compe_brand
	  remove_column :recommendations, :rec_recom_price
	  remove_column :recommendations, :rec_quan_order
  end
end
