class RemoveColunsInItinerary < ActiveRecord::Migration[6.0]
	def change
		remove_columns :itineraries,  :iti_period, :iti_place, :iti_account, :iti_person_visit, :iti_complete
	end
end
