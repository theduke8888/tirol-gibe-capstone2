class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.string :item_name
      t.decimal :item_stand_price, precision: 6, scale: 2
      t.decimal :item_compe_price, precision: 6, scale: 2
      t.string :item_compe_brand
      t.decimal :item_recom_price, precision: 6, scale: 2
      t.integer :item_quan_order
      t.timestamps
    end
  end
end
