class EditColumnNoParticipantsActivity < ActiveRecord::Migration[6.0]
  def change
    change_column :activities, :act_venue_participants, :integer, default: 0
  end
end
