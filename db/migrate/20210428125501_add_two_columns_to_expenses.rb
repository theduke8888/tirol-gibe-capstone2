class AddTwoColumnsToExpenses < ActiveRecord::Migration[6.0]
  def change
	  add_column :expenses, :exp_rejected, :boolean, default: false
	  add_column :expenses, :exp_status, :boolean, default: false
  end
end
