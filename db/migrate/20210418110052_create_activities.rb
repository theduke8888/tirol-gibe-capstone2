class CreateActivities < ActiveRecord::Migration[6.0]
  def change
    create_table :activities do |t|
      t.string :act_rep_name
      t.string :act_rep_title
      t.string :act_rep_department
      t.string :act_rep_head
      t.string :act_event_activity
      t.string :act_event_objective
      t.string :act_event_product_line_up
      t.string :act_event_target
      t.string :act_event_time_frame
      t.string :act_venue_place
      t.string :act_venue_department
      t.date :act_venue_date
      t.integer :act_venue_participants
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
