class AddForeignKeyInItem < ActiveRecord::Migration[6.0]
  def change
	  add_reference :items, :recommendation, foreign_key: true
  end
end
