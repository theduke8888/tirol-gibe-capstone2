class EditColumnsActivity < ActiveRecord::Migration[6.0]
  def change
    remove_column :activities, :act_venue_date
    remove_column :activities, :act_date_request
    add_column :activities, :act_venue_date, :timestamp
    
  end
end
