class AddColumnToItinerary < ActiveRecord::Migration[6.0]
  def change
    add_column :itineraries, :complete, :boolean, default: false
  end
end
