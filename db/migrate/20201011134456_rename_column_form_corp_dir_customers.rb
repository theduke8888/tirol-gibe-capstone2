class RenameColumnFormCorpDirCustomers < ActiveRecord::Migration[6.0]
  def change
    rename_column :customers, :form_cor_dir, :form_corp_dir

  end
end
