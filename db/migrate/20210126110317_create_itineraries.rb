class CreateItineraries < ActiveRecord::Migration[6.0]
  def change
    create_table :itineraries do |t|
      t.date :iti_period_from
      t.date :iti_period_to
      t.date :iti_period
      t.string :iti_place
      t.string :iti_account
      t.string :iti_person_visit
      t.boolean :iti_status, default: false
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
