class RenameColumnItinerary < ActiveRecord::Migration[6.0]
  def change
    rename_column :itineraries, :complete, :iti_complete
  end
end
