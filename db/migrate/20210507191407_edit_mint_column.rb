class EditMintColumn < ActiveRecord::Migration[6.0]
  def change
	  change_column :users, :mint, :string, default: nil
  end
end
