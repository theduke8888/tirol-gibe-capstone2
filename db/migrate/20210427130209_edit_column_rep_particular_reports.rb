class EditColumnRepParticularReports < ActiveRecord::Migration[6.0]
  def change
	  change_column :reports, :rep_particular, :string
  end
end
