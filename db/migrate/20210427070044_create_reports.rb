class CreateReports < ActiveRecord::Migration[6.0]
  def change
    create_table :reports do |t|
      t.date :rep_date
      t.decimal :rep_particular, precision: 8, scale: 2
      t.decimal :rep_transpo, precision: 8, scale: 2
      t.decimal :rep_promo, precision: 8, scale: 2
      t.decimal :rep_repre, precision: 8, scale: 2
      t.decimal :rep_mis, precision: 8, scale: 2
      t.references :expense, null: false, foreign_key: true

      t.timestamps
    end
  end
end
