class CreateRecommendations < ActiveRecord::Migration[6.0]
  def change
    create_table :recommendations do |t|
      t.string :rec_account_name
      t.string :rec_contact_person
      t.string :rec_item
      t.float :rec_stand_price
      t.float :rec_compe_price
      t.string :rec_compe_brand
      t.float :rec_recom_price
      t.integer :rec_quan_order
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
