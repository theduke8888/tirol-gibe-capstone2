class AddColumnsToRecommendationsStatus < ActiveRecord::Migration[6.0]
  def change
    add_column :recommendations, :rec_rejected, :boolean, default: false
    add_column :recommendations, :rec_status, :boolean, default: false
  end
end
