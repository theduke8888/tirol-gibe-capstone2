class AddRemoveIndexEspense < ActiveRecord::Migration[6.0]
  def change
    remove_index :expenses, :exp_tcn
    add_index :expenses, :exp_tcn, unique: true
  end
end
