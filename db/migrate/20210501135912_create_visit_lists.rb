class CreateVisitLists < ActiveRecord::Migration[6.0]
  def change
    create_table :visit_lists do |t|
      t.date :visit_date
      t.string :visit
      t.string :visit_account
      t.string :visit_person
      t.boolean :visit_completed, default: false
      t.references :itinerary, null: false, foreign_key: true

      t.timestamps
    end
  end
end
