class CreateSupports < ActiveRecord::Migration[6.0]
  def change
    create_table :supports do |t|
      t.string :sup_materials
      t.integer :sup_quantity
      t.float :sup_budget, :precision => 8, :scale => 2
      t.string :sup_specification
      t.references :activity, null: false, foreign_key: true

      t.timestamps
    end
  end
end
