class Recommendation < ApplicationRecord
	belongs_to :user
	has_many :items, dependent: :destroy
	
	accepts_nested_attributes_for :items, allow_destroy: true, reject_if: :all_blank
	
	validates :rec_contact_person, presence: true
	validates :rec_account_name, presence: true
	
end
