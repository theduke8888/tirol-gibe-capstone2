class Expense < ApplicationRecord
	belongs_to :user
	has_many :reports, dependent: :destroy
	accepts_nested_attributes_for :reports, allow_destroy: true, reject_if: :all_blank
	
	validates :exp_period_to, presence: true
	validates :exp_period_from, presence: true
=begin
	after_save :generate_tcn
=end


=begin
	private
		def generate_tcn
			tcn = Expense.last
			update_column(:exp_tcn, "TCN#{Date.today.year}-#{tcn.id}")
		end
=end

end

