class Customer < ApplicationRecord
	validates :company, presence: true
	validates :business_address, presence: true
	validates :tel_no, presence: true
	validates :business_location, presence: true
	before_save :first_letter_up
	private

	def first_letter_up
		self.company = company.titleize
		self.business_address = business_address.titleize
		self.residential_address = residential_address.titleize
		self.type_business = type_business.titleize
		self.product_lines  =  product_lines.titleize
		self.industry_affiliation = industry_affiliation.titleize
		self.position = position.titleize
		self.form_owner_name = form_owner_name.titleize unless self.form_owner_name.nil?
		self.form_partner = form_partner.titleize unless self.form_partner.nil?
		self.form_corp_pres = form_corp_pres.titleize unless self.form_corp_pres.nil?
		self.form_corp_vp = form_corp_vp.titleize unless self.form_corp_vp.nil?
		self.form_corp_sec = form_corp_sec.titleize unless self.form_corp_sec.nil?
		self.form_corp_tres = form_corp_tres.titleize unless self.form_corp_tres.nil?
		self.form_corp_dir = form_corp_dir.titleize unless self.form_corp_dir.nil?
	end
end
