class Itinerary < ApplicationRecord
	belongs_to :user
	has_many :visit_lists, dependent: :destroy
	accepts_nested_attributes_for :visit_lists, allow_destroy: true, reject_if: :all_blank
	validates :iti_period_from, presence: true
	validates :iti_period_to, presence: true
end
