class Product < ApplicationRecord
	belongs_to :category
	validates :name, presence: true
#	validates :sku, presence: true, uniqueness: true
	default_scope -> {order(name: :asc)}
	before_save {self.name = name.titleize}
	mount_uploader :image, ProductUploader
end
