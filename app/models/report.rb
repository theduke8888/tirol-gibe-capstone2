class Report < ApplicationRecord
	belongs_to :expense, optional: true
	validates :rep_date,  presence: true
	validates :rep_particular, presence: true
	validates :rep_promo, presence: true
	validates :rep_repre, presence: true
	validates :rep_mis, presence: true
end
