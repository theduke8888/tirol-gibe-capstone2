class User < ApplicationRecord
	belongs_to :job
#	has_many :itineraries, dependent: :destroy
	has_many :itineraries, dependent: :destroy
	has_many :recommendations, dependent: :destroy
	has_many :activities, dependent: :destroy
	has_many :expenses, dependent: :destroy
# delete this something
	has_many :interests,:through => :activities
	
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable
	validates :lastname, presence: true
	validates :firstname, presence: true

	validates :contactnumber, presence: true
	mount_uploader :image, ImageUploader
	before_validation  :setPassword


	private
	def setPassword
		if new_record?
			self.password = "123456"
			self.password_digest "123456"
		end
	end
end
