class Category < ApplicationRecord
	has_many :products, dependent: :destroy
	validates :name, presence: true
	before_save {self.name = name.titlecase}
	mount_uploader :image, ImageCategoryUploader
end
