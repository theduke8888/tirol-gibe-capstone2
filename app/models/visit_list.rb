class VisitList < ApplicationRecord
	belongs_to :itinerary
	validates :visit_date, presence: true
	validates :visit_place, presence: true
	validates :visit_account, presence: true
	validates :visit_person, presence: true
end
