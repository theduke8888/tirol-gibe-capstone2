class Activity < ApplicationRecord
  belongs_to :user
  has_many :interests, dependent: :destroy
  has_many :supports, dependent: :destroy
  
  accepts_nested_attributes_for :interests, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :supports, allow_destroy: true, reject_if: :all_blank
  
=begin
  after_save :generate_control_no
=end
  validates :act_date_needed, presence: true
  validates :act_rep_name, presence: true
  validates :act_rep_department, presence: true
  validates :act_rep_head,presence: true
  validates :act_event_activity, presence: true
  validates :act_event_objective, presence: true
  validates :act_event_target, presence: true
  validates :act_event_time_frame, presence: true
  validates :act_venue_place, presence: true
  validates :act_venue_department, presence: true
  validates :act_venue_date, presence: true
  
=begin
  private
  def generate_control_no
    control_no = Activity.last
    update_column(:act_control_no, "#{Date.today.year}-#{control_no.id}")
    
      # update_column(:act_rep_title, control_no.job.title)
  end
=end
end
