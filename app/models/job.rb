class Job < ApplicationRecord
     has_many :users, dependent: :destroy
     validates :title, presence: true
     before_save {self.title = title.titlecase}

end
