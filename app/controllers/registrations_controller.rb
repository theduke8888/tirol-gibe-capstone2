class RegistrationsController < Devise::RegistrationsController
     protected

          def after_sign_up_path_for(resource)
               sign_out
               root_url
          end

          def after_inactive_sign_up_path_for(resource)
               sign_out
               root_url
		end
		
		
end
