class ItinerariesController < ApplicationController
     protect_from_forgery with: Exception
	before_action :no_owner, only: [:edit]
	
	def index
     	if current_user.admin? or current_user.admin2?
			@itineraries = Itinerary.paginate(page: params[:page]).order(created_at: :desc)
			@status = Itinerary.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
			@itineraries_admin = current_user.itineraries.paginate(page: params[:page]).order(created_at: :desc)
			@status_admin = current_user.itineraries.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
		else
			@itineraries = current_user.itineraries.paginate(page: params[:page]).order(created_at: :desc)
			@status = current_user.itineraries.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
		end
	
     end
     
	def show
          @itinerary = Itinerary.find(params[:id])
     end

     def new
		@itinerary_last = current_user.itineraries.last
	     @itinerary = current_user.itineraries.new
	end

     def create
		@itinerary = current_user.itineraries.build(itinerary_params)
          if @itinerary.save
			@itinerary.update_columns(iti_created: true)
               flash[:success] = "Itenerary created..."
               redirect_to @itinerary
          else
               render 'new'
          end
     end

     def edit
          @itinerary = if current_user.admin? || current_user.admin2?
					   Itinerary.find(params[:id])
				   else
					   current_user.itineraries.find(params[:id])
				   end
	end

     def update
          @itinerary = if current_user.admin? || current_user.admin2?
					   Itinerary.find(params[:id])
				   else
					   current_user.itineraries.find(params[:id])
				   end
			 
          if @itinerary.update(itinerary_params)
			@itinerary.update(iti_rejected: false)
		     flash[:success] = "Itinerary modified"
               redirect_to @itinerary
          else
               render "edit"
          end

     end

     def destroy
		@itinerary = if current_user.admin? || current_user.admin2?
					   Itinerary.find(params[:id])
				   else
					   current_user.itineraries.find(params[:id]).destroy
				   end
		  
          if @itinerary.destroy
               flash[:danger] = "Itinerary deleted"
               redirect_to itineraries_url
          else
               flash[:warning] = "Error in delete"
               redirect_to itineraries_url
          end
     end
  	
	def update_status_visit_list
		@visit = VisitList.find(params[:id])
		if @visit.update(visit_completed: true)
			redirect_to itinerary_url @visit.itinerary
			flash[:success] = "Status updated..."
		end
	end
	
	def current_week
		check_admin_admin2
		@itineraries = Itinerary.where("(iti_period_from >= ? and iti_period_to <= ?) and iti_status = ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week, false).paginate(page: params[:page]).order(created_at: :desc)
		@status = Itinerary.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
		@itineraries_admin = current_user.itineraries.paginate(page: params[:page]).order(created_at: :desc)
		@status_admin = current_user.itineraries.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
	end
	
	def approved
		check_admin_admin2
		@itineraries = Itinerary.where("(iti_period_from >= ? and iti_period_to <= ?) and iti_status = ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week, true).paginate(page: params[:page]).order(created_at: :desc)
		@status = Itinerary.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
		@itineraries_admin = current_user.itineraries.paginate(page: params[:page]).order(created_at: :desc)
		@status_admin = current_user.itineraries.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
	
	end
	
	def rejected
		check_admin_admin2
		@itineraries = Itinerary.where("(iti_period_from >= ? and iti_period_to <= ?) and iti_rejected = ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week, true).paginate(page: params[:page]).order(created_at: :desc)
		@status = Itinerary.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
		@itineraries_admin = current_user.itineraries.paginate(page: params[:page]).order(created_at: :desc)
		@status_admin = current_user.itineraries.where("iti_period_from >= ? and iti_period_to <= ?", Time.zone.now.beginning_of_week, Time.zone.now.end_of_week).all
	end
	
	def approve
		check_admin_admin2
		@itinerary = Itinerary.find(params[:id])
		if @itinerary.update(iti_status: true, iti_rejected: false)
			flash[:success] = "Itinerary approved successfully..."
			redirect_to current_week_itineraries_url
		else
			flash[:danger] = "Error in approving itinerary..."
			redirect_to current_week_itineraries_url
		end
	end
	
	def reject
		check_admin_admin2
		@itinerary = Itinerary.find(params[:id])
		if @itinerary.update(iti_rejected: true)
			flash[:success] = "Itinerary rejected successfully"
			redirect_to current_week_itineraries_url
		else
			flash[:danger]  = "Error in rejecting itinerary"
			redirect_to current_week_itineraries_url
		end
	end
		
     private
     def itinerary_params
		params.require(:itinerary).permit(:iti_period_from, :iti_period_to,
								    visit_lists_attributes: VisitList.attribute_names.map(&:to_sym).push(:_destroy))
	end
	
	def no_owner
		unless  current_user.admin? or current_user.admin2
			@itinerary = current_user.itineraries.find_by(id: params[:id])
			if @itinerary.nil?
				flash[:warning] = "You don't have a  previlige"
				redirect_to itineraries_url
			end
		
		end
		
	end
end
