class CategoriesController < ApplicationController
	protect_from_forgery with: Exception

	def index
		@categories = Category.all.order(:name)
	end

	def show
		@category = Category.find(params[:id])
	end

	def new
		@category = Category.new
	end

	def create
		@category = Category.new(category_params)
		if @category.save
			flash[:success] = "Caregory Added"
			redirect_to categories_url
		else
			render 'new'
		end
	end

	def edit
		@category = Category.find(params[:id])
	end

	def update
		@category = Category.find(params[:id])
		if @category.update_attributes(category_params)
			flash[:success] = "Update Success"
			redirect_to @category
		else
			rende 'edit'
		end
	end

	def destroy
		@category = Category.find(params[:id])
		if @category.destroy
			flash[:danger] = "Category Deleted"
			redirect_to categories_url
		else
			flash[:warning] = "Error in delete"
			redirect_to @category
		end
	end

	private
	def category_params
		params.require(:category).permit(:name, :image)
	end
end
