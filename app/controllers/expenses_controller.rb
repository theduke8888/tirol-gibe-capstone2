class ExpensesController < ApplicationController
	protect_from_forgery with: Exception
	before_action :no_owner, only: [:edit]
	
	def index
		if current_user.admin? || current_user.admin2?
			@expenses = Expense.paginate(page: params[:page]).order(created_at: :desc)
			@expenses_admin = current_user.expenses.paginate(page: params[:page]).order(created_at: :desc)
		else
			@expenses = current_user.expenses.paginate(page: params[:page]).order(created_at: :desc)
		end
	end
	
	def show
		@expense = Expense.find(params[:id])
	end
	
	def new
		@expense = current_user.expenses.new
	end
	
	def create
		@expense = current_user.expenses.build(expense_params)
		if @expense.save
			flash[:success] = "Report of Travel and Other Incidental Expenses Added Successfully"
			@expense.update(exp_tcn: "TCN#{Date.today.year}-#{@expense.id}")
			redirect_to @expense
		else
			render :new
		end
	end
	
	def edit
		@expense = if current_user.admin? || current_user.admin2?
					 Expense.find(params[:id])
				 else
					 current_user.expenses.find(params[:id])
				 end
	end
	
	def update
		
		@expense = if current_user.admin? || current_user.admin2?
					 Expense.find(params[:id])
				 else
					 current_user.expenses.find(params[:id])
				 end
		if @expense.update(expense_params)
			@expense.update(exp_rejected: false)
			flash[:success] = "Report of Travel and Other Incidental Expenses Modified Successfully"
			redirect_to @expense
		else
			render :edit
		end
	end
	
	def destroy
		@expense = if current_user.admin? || current_user.admin2?
					 Expense.find(params[:id])
				 else
					 current_user.expenses.find(params[:id])
				 end
		if @expense.destroy
			flash[:success] = "Report of Travel and Other Incidental Expenses Deleted Successfully"
			redirect_to expenses_url
		end
		
	end
	
	def search_tcn_no
		result = Expense.find_by(exp_tcn: params[:search].upcase)
		if result
			redirect_to expense_url result
		else
			flash[:danger] = "TCN No not found..."
			redirect_to expenses_url
		end
	end
	
	def for_approval
		check_admin_admin2
		@expenses = Expense.where(exp_status: false).paginate(page: params[:page]).order(created_at: :desc)
		@expenses_admin = current_user.expenses.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def approved
		check_admin_admin2
		@expenses = Expense.where(exp_status: true).paginate(page: params[:page]).order(created_at: :desc)
		@expenses_admin = current_user.expenses.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def rejected
		check_admin_admin2
		@expenses = Expense.where(exp_rejected: true).paginate(page: params[:page]).order(created_at: :desc)
		@expenses_admin = current_user.expenses.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def approve
		check_admin_admin2
		@expense = Expense.find(params[:id])
		if @expense.update(exp_status: true, exp_rejected: false)
			flash[:success] = "Report for travel and other incidental expenses approved successfully..."
			redirect_to for_approval_expenses_url
		else
			flash[:danger] = "Error in approving report for travel and other incidental expenses..."
			redirect_to for_approval_expenses_url
		end
	end
	
	def reject
		check_admin_admin2
		@expense = Expense.find(params[:id])
		if @expense.update(exp_rejected: true)
			flash[:success] = "Report for travel and other incidental expenses rejected successfully..."
			redirect_to for_approval_expenses_url
		else
			flash[:danger] = "Error in rejecting report for travel and other incidental expenses..."
			redirect_to for_approval_expenses_url
		end
	end
	
	private
		def expense_params
			params.require(:expense).permit(:exp_period_from, :exp_period_to,
									  reports_attributes: Report.attribute_names.map(&:to_sym).push(:_destroy))
		end
	
		def no_owner
			unless current_user.admin? || current_user.admin2?
				@expense = current_user.expenses.find_by(id: params[:id])
				if @expense.nil?
					flash[:warning] = "You don't have a previlige"
					redirect_to expenses_url
				end
			end
		end
end

