class JobsController < ApplicationController
     protect_from_forgery with: Exception
	before_action :check_admin2, only: [:edit, :new]
	
     def index
          @jobs = Job.all.order(:title)
     end

     def show
          @job = Job.find(params[:id])
          @jobs = Job.all.order(:title)
          
     end

     def new
          @job = Job.new
          @jobs = Job.all
     end

     def create
          @job  = Job.new(job_params)
          @jobs = Job.all
          if @job.save
               redirect_to jobs_url
          else
               render 'new'
          end
     end

     def edit
          @job = Job.find(params[:id])
          @jobs = Job.all
     end

     def update
          @job = Job.find(params[:id])
          @jobs = Job.all

          if @job.update_attributes(job_params)
               redirect_to jobs_url
          else
               render 'edit'
          end
     end

     def destroy
          @job = Job.find(params[:id])

          if @job.destroy
               redirect_to jobs_url
          else
               render json: {error: @job.error.messages}, status: 422
          end
     end


     private
          def job_params
               params.require(:job).permit(:title)
          end
end
