class ApplicationController < ActionController::Base
     protect_from_forgery with: :exception

     before_action :configure_permitted_parameters,  if: :devise_controller?
	before_action :authenticate_user!
	
     protected

          def configure_permitted_parameters
               devise_parameter_sanitizer.permit(:sign_up, keys: [:lastname, :firstname, :mint, :contactnumber, :admin, :job_id, :address])
               devise_parameter_sanitizer.permit(:account_update, keys: [:lastname, :firstname, :mint, :contactnumber, :admin, :job_id, :address,:image])
          end

          def check_signed_in
               unless user_signed_in?
                    redirect_to new_user_session_url
               end
		end

		
		
		def after_sign_in_path_for(resource_or_scope)
			if resource_or_scope.admin2? or resource_or_scope.admin?
				dashboards_url
			else
				root_url
			end
		end
		
		def check_admin
			unless current_user.admin?
				redirect_to itineraries_url
			end
		end
		
		def check_admin2
			unless current_user.admin2?
				flash[:warning] = "You don't have a privilege"
				redirect_to itineraries_url
			end
		end
	
		def check_admin_admin2
=begin
			if current_user.admin?
				redirect_to dashboards_url
			elsif current_user.admin2?
				redirect_to dashboards_url
			else
				redirect_to itineraries_url
			end

=end
			unless current_user.admin? or current_user.admin2?
				redirect_to itineraries_url
			end
		end
	
end
