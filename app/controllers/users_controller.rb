class UsersController < ApplicationController
	def index
		@users = User.order(:lastname).all
	end
	
	def new
		if current_user.admin2?
			@user = User.new
		else
			flash[:warning] = "You don't have a prelivige"
			redirect_to itineraries_url
		end
	end
	
	def create
		@user = User.new(user_params)
		if @user.save
			flash[:success] = "Successfully added a user"
			redirect_to @user
		else
			render 'new'
		end
	end
	
	def show
		@user = User.find(params[:id])
	end

	def edit
		if current_user.admin2?
			@user = User.find(params[:id])
		else
			flash[:warning] = "You don't have a prelivige"
			redirect_to itineraries_url
		end
	end
	
	def update
		if current_user.admin2?
			@user = User.find(params[:id])
			if @user.update(user_params)
				flash[:success] = "Profile edite successfully"
				redirect @user
			else
				render 'edit'
			end
		end
	end
	
	def destroy
		User.find(params[:id]).destroy
		flash[:success] = 'User deleted'
		redirect_to users_path
	end

	def show_cucurent_user
		@user = User.find(params[:id])
	end
	
	def user_params
		params.require(:user).permit(:lastname, :firstname, :mint, :contactnumber, :address, :email, :job_id)
	end
	
	
end
