class ActivitiesController < ApplicationController
	protect_from_forgery with: Exception
	before_action :no_owner, only: [:edit]
	
	def index
		if current_user.admin? || current_user.admin2?
			@activities = Activity.paginate(page: params[:page]).order(created_at: :desc)
			@activities_admin = current_user.activities.paginate(page: params[:page]).order(created_at: :desc)
		else
			@activities = current_user.activities.paginate(page: params[:page]).order(created_at: :desc)
		end
	end
	
	def show
		@activity = Activity.find(params[:id])
		@interests  = @activity.interests.all
		@supports = @activity.supports.all
	end
	
	def new
		@activity = Activity.new
	end
	
	def create
		@activity = current_user.activities.build(activity_params)
		if @activity.save
			flash[:success] = "Activity added successfuly"
			@activity.update(act_control_no: "ACN#{Date.today.year}-#{@activity.id}")
			redirect_to @activity
		else
			render 'new'
		
		end
	end
	
	def edit
		@activity = if current_user.admin? || current_user.admin2?
					  Activity.find(params[:id])
			  	  else
					  current_user.activities.find(params[:id])
				  end
					 
	end
	
	def update
		@activity = if current_user.admin? || current_user.admin2?
					  Activity.find(params[:id])
				  else
					  current_user.activities.find(params[:id])
				  end
			 
		if @activity.update_attributes(activity_params)
			@activity.update(act_rejected: false)
			flash[:success] = "Activity update successfully"
			redirect_to @activity
		else
			render :edit
		end
	
	end
	
	def destroy
		@activity = if current_user.admin? || current_user.admin2?
					  Activity.find(params[:id])
				  else
					  current_user.activities.find(params[:id])
				  end
		if @activity.destroy
			flash[:success] = "Activity deleted successfully"
		end
	
		redirect_to activities_url
	end
	
	def search_control_no
		result = Activity.find_by_act_control_no(params[:search])
		if result
			redirect_to activity_url result
		else
			flash[:warning] = "Control number not found"
			redirect_to activities_url
		
		end
	end
	
	def for_approval
		check_admin_admin2
		@activities = Activity.where(act_status: false).paginate(page: params[:page]).order(created_at: :desc)
		@activities_admin = current_user.activities.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def approved
		check_admin_admin2
		@activities = Activity.where(act_status: true).paginate(page: params[:page]).order(created_at: :desc)
		@activities_admin = current_user.activities.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def rejected
		check_admin_admin2
		@activities = Activity.where(act_rejected: true).paginate(page: params[:page]).order(created_at: :desc)
		@activities_admin = current_user.activities.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def approve
		check_admin_admin2
		@expense = Activity.find(params[:id])
		if @expense.update(act_status: true, act_rejected: false)
			flash[:success] = "Activity approved successfully..."
			redirect_to for_approval_activities_url
		else
			flash[:danger] = "Error in rejecting activity..."
			redirect_to for_approval_activities_url
		end
	end
	
	def reject
		check_admin_admin2
		@expense = Activity.find(params[:id])
		if @expense.update(act_rejected: true)
			flash[:success] ="Activity rejected successfully..."
			redirect_to for_approval_activities_url
		else
			flash[:danger] = "Error in rejecting activity..."
			redirect_to for_approval_activities_url
		
		end
	end
	private
		def activity_params
			params.require(:activity).permit(:act_date_needed, :act_rep_name, :act_rep_department, :act_rep_title,
									   :act_rep_head, :act_event_activity, :act_event_objective, :act_event_target,
									   :act_event_target, :act_event_time_frame, :act_venue_place, :act_venue_department,
									   :act_event_product_line_up, :act_venue_date,
									   interests_attributes: Interest.attribute_names.map(&:to_sym).push(:_destroy),
									   supports_attributes: Support.attribute_names.map(&:to_sym).push(:_destroy))
		end
	
		def no_owner
			unless current_user.admin? || current_user.admin2?
				@activity = current_user.activities.find_by(id: params[:id])
				if @activity.nil?
					flash[:warning] = "You don't have a  previlige"
					redirect_to activities_url
				end
			end
		end
		
end
