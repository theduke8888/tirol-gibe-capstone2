class RecommendationsController < ApplicationController
	protect_from_forgery with: Exception
	before_action :now_owner, only: [:edit]
	
	def index
		if current_user.admin? || current_user.admin2?
			@recommendations = Recommendation.paginate(page: params[:page]).order(created_at: :desc)
			@recommendations_admin = current_user.recommendations.paginate(page: params[:page]).order(created_at: :desc)
		else
			@recommendations = current_user.recommendations.paginate(page: params[:page]).order(created_at: :desc)
		end
	
	end
	
	def new
		@recommendation = current_user.recommendations.new
	end
	
	def create
		@recommendation = current_user.recommendations.build(recommendation_params)
		if @recommendation.save
			flash[:success] = "Price recommendation saved"
			redirect_to recommendations_url
		else
			render 'new'
		end
	end
	def show
		@recommendation = Recommendation.find(params[:id])
	end
	
	def edit
		@recommendation = current_user.admin? || current_user.admin2? ? Recommendation.find(params[:id]) : current_user.recommendations.find(params[:id])
	end
	
	def update
		@recommendation = current_user.admin? || current_user.admin2? ? Recommendation.find(params[:id]) : current_user.recommendations.find(params[:id])
		if @recommendation.update_attributes(recommendation_params)
			@recommendation.update(rec_rejected: false)
			flash[:success] = "Price recommendation updated successfully"
			redirect_to @recommendation
		else
			render 'edit'
		end
	end
	
	def destroy
		@recommendation = current_user.admin? || current_user.admin2? ? Recommendation.find(params[:id]) : current_user.recommendations.find(params[:id])
		if @recommendation.destroy
			flash[:success] = "Price recommendation deleted successfully"
			redirect_to recommendations_url
		end
	
	end
	
	def for_approval
		check_admin_admin2
		@recommendations = Recommendation.where(rec_status: false).paginate(page: params[:page]).order(created_at: :desc)
		@recommendations_admin = current_user.recommendations.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def approved
		check_admin_admin2
		@recommendations = Recommendation.where(rec_status: true).paginate(page: params[:page]).order(created_at: :desc)
		@recommendations_admin = current_user.recommendations.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def rejected
		check_admin_admin2
		@recommendations = Recommendation.where(rec_rejected:  true).paginate(page: params[:page]).order(created_at: :desc)
		@recommendations_admin = current_user.recommendations.paginate(page: params[:page]).order(created_at: :desc)
	end
	
	def approve
		check_admin_admin2
		@recommendation = Recommendation.find(params[:id])
		if @recommendation.update(rec_status: true, rec_rejected: false)
			flash[:success] = "Price recommendation approved successully..."
			redirect_to for_approval_recommendations_url
		else
			flash[:danger] = "Error in approving recommendation"
			redirect_to for_approval_recommendations_url
		end
	end
	
	def reject
		check_admin_admin2
		@recommendation = Recommendation.find(params[:id])
		if @recommendation.update(rec_rejected: true)
			flash[:success] = "Price recommendation rejected successully..."
			redirect_to for_approval_recommendations_url
		else
			flash[:success] = "Error in rejecting price recommendation..."
			redirect_to rejected_recommendations_url
		end
	end
	
	private
		def recommendation_params
			params.require(:recommendation).permit(:rec_account_name, :rec_contact_person, :rec_terms,
										    items_attributes: Item.attribute_names.map(&:to_sym).push(:_destroy))
		end
		
		def now_owner
			unless current_user.admin? || current_user.admin2?
				@recommendation = current_user.recommendations.find_by(id: params[:id])
				if @recommendation.nil?
					flash[:warning] = "You don't have a previlige"
					redirect_to recommendations_url
				end
			end
		end

end
