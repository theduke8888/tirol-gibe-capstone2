class ProductsController < ApplicationController
	protect_from_forgery with: Exception
	before_action :check_admin2, only: [:new, :edit]

	def index
		@products = Product.paginate(page: params[:page])
	end

	def show
		@product = Product.find(params[:id])
	end

	def new
		@product = Product.new
		@products = Product.paginate(page: params[:page], per_page: 12)
	end

	def create
		@products = Product.paginate(page: params[:page], per_page: 12)
		@product = Product.new(product_params)
		if @product.save
			flash.now[:success] = 'New Product Added'
			redirect_to @product
		else
			render 'new'
		end
	end

	def edit
		@product = Product.find(params[:id])
	end

	def update
		@product = Product.find(params[:id])
		if @product.update_attributes(product_params)
			flash[:success] = 'Product Updated'
			redirect_to @product
		else
			render 'edit'
		end
	end

	def destroy
		@product  = Product.find(params[:id])
		if @product.destroy
			flash[:danger] = 'Product Deleted'
			redirect_to products_url
		else
			flash[:warning] = 'Error during delete'
		end
	end

	private

	def product_params
		params.require(:product).permit(:name, :sku, :image, :category_id)
	end
end
