class CustomersController < ApplicationController
	protect_from_forgery with: Exception
	before_action :check_admin2, only: [:edit, :new]
	def index
		@customers = Customer.paginate(page: params[:page], per_page: 30)
	end

	def show
		@customer = Customer.find(params[:id])
	end

	def new
		@customer = Customer.new
	end

	def create
		@customer = Customer.new(customer_params)
		if @customer.save
			flash[:success] = "Successfully Address New Customer"
			redirect_to @customer
		else
			render 'new'
		end
	end

	def edit
		@customer = Customer.find(params[:id])
	end

	def update
		@customer = Customer.find(params[:id])
		if @customer.update_attributes(customer_params)
			flash[:success] = "Modified Customer Successfully"
			redirect_to @customer
		else
			render 'edit'
		end

=begin
		@customer = Customer.find(params[:id])
		if @customer.update_attributes(customer_params)
			flas.now[:success] = "Successully Existing Customer"
			redirect_to @customer
		else
			render 'edit'
		end
=end
	end

	def destroy
=begin
		Customer.find(params[:id]).destroy
		flash[:danger] = "Customer deleted"
		redirect_to customers_url
=end
		Customer.find(params[:id]).destroy
		flash[:danger] = "Customer deleted"
		redirect_to customers_url
	end

	private
	def customer_params
		params.require(:customer).permit(:company, :year_establised, :no_employees, :business_address,
		                                 :tel_no, :fax_no, :business_location, :business_year_stay, :residential_address, :residential_tel_no,
		                                 :residential_location, :residential_year_stay,  :type_business, :product_lines,
		                                 :industry_affiliation, :position,:form_owner_name, :form_partner, :form_corp_pres,
		                                 :form_corp_vp, :form_corp_sec, :form_corp_tres, :form_corp_dir )
	end
end
